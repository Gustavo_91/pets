# Pets

Find pets to adopt in New York

## Getting Started

1. Run a Pod install command in the root directory
2. Open Pets workspace in Xcode 11 (The file with .xcworkspace)
3. Build and Run the **Pets** target in a device or simulator with iOS >= 12.0

### Prerequisites

* Xcode 11
* Swift 5.1

## Author

* **Gustavo Pirela**