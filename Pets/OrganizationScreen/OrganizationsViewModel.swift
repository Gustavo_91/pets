//
//  OrganizationsViewModel.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import UIKit
import MapKit

class OrganizationsViewModel {
    var organizations = [Organization]()
    let networkClient = NetworkClient()
    
    func configure(view: MKMapView) {
        var annotations = [MKAnnotation]()
        organizations.forEach {
            let annotation = OrganizationAnnotation(organization: $0)
            annotations.append(annotation)
        }
        
        view.showAnnotations(annotations, animated: true)
    }
    
    func fetchOrganizations(view: MKMapView) {
        networkClient.fetchOrganizations { organizations in
            self.organizations = organizations
            self.configure(view: view)
        }
    }
}
