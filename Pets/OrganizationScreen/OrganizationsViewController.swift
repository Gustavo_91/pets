//
//  MapViewController.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import UIKit
import MapKit

class OrganizationsViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    let viewModel = OrganizationsViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        viewModel.fetchOrganizations(view: mapView)
    }
}

extension OrganizationsViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let view = mapView.dequeueReusableAnnotationView(withIdentifier: String(describing: MKMarkerAnnotationView.self))
        return view
    }
}

private extension OrganizationsViewController {
    func setUI() {
        setMapRegion()
        mapView.delegate = self
        mapView.register(MKMarkerAnnotationView.self,
                         forAnnotationViewWithReuseIdentifier: String(describing: MKMarkerAnnotationView.self))
    }
    
    func setMapRegion() {
        let coordinates = Constant.Map.center.coordinate
        let center = CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)
        let region = MKCoordinateRegion(center: center, span: span)
        mapView.region = region
    }
}
