//
//  Organization.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import Foundation
import MapKit

struct Organization: Decodable {
    let name: String
    let distance: Double
}
