//
//  OrganizationAnnotation.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import Foundation
import MapKit

class OrganizationAnnotation: NSObject {
    let organization: Organization
    private static var degree = 0.0
    
    init(organization: Organization) {
        self.organization = organization
    }
}

extension OrganizationAnnotation: MKAnnotation {
    var coordinate: CLLocationCoordinate2D {
        let location = organizationLocation
        OrganizationAnnotation.degree += 2 * .pi / 10
        return organizationLocation.coordinate
    }
    
    var title: String? {
        return organization.name
    }
}

private extension OrganizationAnnotation {
    var organizationLocation: CLLocation {
        let earthRadius = 6378.1 * 0.621371
        let bearing = OrganizationAnnotation.degree
        let coordinates = Constant.Map.center.coordinate
        let lat1 = coordinates.latitude * .pi / 180
        let lon1 = coordinates.longitude * .pi / 180
        let distance = organization.distance
        
        let lat2 = asin(sin(lat1) * cos(distance/earthRadius)
                        + cos(lat1) * sin(distance/earthRadius)
                        * cos(bearing))
        
        let lon2 = lon1 + atan2(sin(bearing) * sin(distance/earthRadius) * cos(lat1),
                                cos(distance/earthRadius) - sin(lat1) * sin(lat2))
        
        return CLLocation(latitude: lat2 * 180 / .pi , longitude: lon2 * 180 / .pi)
    }
}
