//
//  Animal.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import Foundation

struct Pet {
    let name: String
    let gender: String
    let age: String
    let type: String
    let photoUrl: URL?
}

extension Pet: Decodable {
    enum CodingKeys: String, CodingKey {
        case name
        case gender
        case age
        case photos
        case type
        
        enum PhotoKeys: String, CodingKey {
            case small
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.gender = try container.decode(String.self, forKey: .gender)
        self.age = try container.decode(String.self, forKey: .age)
        self.type = try container.decode(String.self, forKey: .type)
        
        var photosContainer = try container.nestedUnkeyedContainer(forKey: .photos)
        var urls = [URL]()
        while !photosContainer.isAtEnd {
            let container = try photosContainer.nestedContainer(keyedBy: CodingKeys.PhotoKeys.self)
            let url = try container.decode(URL.self, forKey: .small)
            urls.append(url)
        }
        
        self.photoUrl = urls.isEmpty ? nil : urls[0]
    }
}
