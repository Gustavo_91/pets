//
//  PetsContainer.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import Foundation

struct PetsContainer: Decodable {
    let pets: [Pet]
    
    enum CodingKeys: String, CodingKey {
        case pets = "animals"
    }
}
