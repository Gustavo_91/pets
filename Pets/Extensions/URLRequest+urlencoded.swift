//
//  URLRequest+urlencoded.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import Foundation

extension URLRequest {
    private func percentEscapeString(_ string: String) -> String {
      var characterSet = CharacterSet.alphanumerics
      characterSet.insert(charactersIn: "-._* ")
      
      return string
        .addingPercentEncoding(withAllowedCharacters: characterSet)!
        .replacingOccurrences(of: " ", with: "+")
        .replacingOccurrences(of: " ", with: "+", options: [], range: nil)
    }
    
    mutating func composeBody(with parameters: [String : String]) {
      let parameterArray = parameters.map { (arg) -> String in
        let (key, value) = arg
        return "\(key)=\(self.percentEscapeString(value))"
      }
      
      httpBody = parameterArray.joined(separator: "&").data(using: String.Encoding.utf8)
    }
}
