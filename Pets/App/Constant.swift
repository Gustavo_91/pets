//
//  Constants.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import Foundation
import MapKit

struct Constant {
    struct Map {
        static let centerLatitude = CLLocationDegrees(40.778532)
        static let centerLongitude = CLLocationDegrees(-73.957451)
        static let center = CLLocation(latitude: centerLatitude, longitude: centerLongitude)
    }
}
