//
//  TableViewCell.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import UIKit

class PetCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var petImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        petImageView.layer.cornerRadius = 15
        petImageView.layer.masksToBounds = true
    }

}
