//
//  PetViewModel.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import UIKit
import Nuke

class PetViewModel {
    var pets = [Pet]()
    var filterState: SearchStatus = .withoutFilter
    let networkClient = NetworkClient()
    var totalPets: Int {
        return shownPets.count
    }

    var tableRows: Int {
        return shownPets.count
    }
    
    func configure(view: PetCell, row: Int) {
        let pets = shownPets
        view.nameLabel.text = pets[row].name
        view.genderLabel.text = pets[row].gender
        view.ageLabel.text = pets[row].age
        view.typeLabel.text = pets[row].type
        if let url = pets[row].photoUrl { Nuke.loadImage(with: url, into: view.petImageView) }
    }
    
    func configure(view: UITableView) {
        reloadToTop(tableView: view)
    }
    
    func fetchPets(view: UITableView) {
        networkClient.fetchPets { pets in
            self.pets += pets
            view.reloadData()
        }
    }
    
}

private extension PetViewModel {
    var shownPets: [Pet] {
        switch filterState {
        case let .withFilter(filter: filter):
            return pets.filter { $0.type.lowercased().contains(filter.lowercased()) }
        case .withoutFilter:
            return pets
        }
    }
    
    private func reloadToTop(tableView: UITableView) {
        tableView.reloadData()
        let indexPath = IndexPath(row: 0, section: 0)
        if shownPets.count > 0 { tableView.scrollToRow(at: indexPath, at: .top, animated: true) }
    }
}

enum SearchStatus {
    case withoutFilter
    case withFilter(filter: String)
}
