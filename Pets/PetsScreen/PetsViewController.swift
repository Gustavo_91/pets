//
//  PetsViewController.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import UIKit
import Nuke

class PetsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    let viewModel = PetViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        viewModel.fetchPets(view: tableView)
    }
}

extension PetsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PetCell.self), for: indexPath) as! PetCell
        viewModel.configure(view: cell, row: indexPath.row)
        return cell
    }
}

extension PetsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 1 == viewModel.totalPets {
            viewModel.fetchPets(view: tableView)
        }
    }
}

extension PetsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let text = searchBar.text else { return }
        viewModel.filterState = text.isEmpty ? .withoutFilter : .withFilter(filter: text)
        viewModel.configure(view: tableView)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let isEmpty = searchBar.text?.isEmpty, !isEmpty {
            searchBar.text = nil
            viewModel.filterState = .withoutFilter
            viewModel.configure(view: tableView)
        }
    }
}

private extension PetsViewController {
    func setUI() {
        setSearchBar()
        setTableView()
    }
    
    func setSearchBar() {
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        if #available(iOS 13.0, *) { searchBar.searchTextField.clearButtonMode = .never }
    }
    
    func setTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        let footerView = createFooterView()
        tableView.tableFooterView = footerView
        tableView.allowsSelection = false
        let nib = UINib.init(nibName: String(describing: PetCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: PetCell.self))
    }
    
    func createFooterView() -> UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 60))
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.style = .whiteLarge
        indicator.color = .black
        indicator.startAnimating()
        footerView.addSubview(indicator)
        NSLayoutConstraint.activate([
            indicator.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
            indicator.centerXAnchor.constraint(equalTo: footerView.centerXAnchor),
        ])
        
        return footerView
    }
}




