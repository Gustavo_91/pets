//
//  NetworkClient.swift
//  Pets
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import Foundation

class NetworkClient {
    var token: Token?
    var petsPage = 1
    private let session = URLSession(configuration: .default)
    private let decoder = JSONDecoder()
    
    func fetchPets(completion: @escaping ([Pet]) -> Void) {
        guard let request = ApiRequest.token.makeTokenRequest() else { return }
        
        let task = session.dataTask(with: request) {data, response, error in
            guard let data = data else { return }
            self.token = try? self.decoder.decode(Token.self, from: data)
            self.fetch(completion: completion)
        }
        
        task.resume()
    }
    
    func fetchOrganizations(completion: @escaping ([Organization]) -> Void) {
        guard let request = ApiRequest.token.makeTokenRequest() else { return }
        
        let task = session.dataTask(with: request) {data, response, error in
            guard let data = data else { return }
            self.token = try? self.decoder.decode(Token.self, from: data)
            self.fetch(completion: completion)
        }
        
        task.resume()
    }
}

private extension NetworkClient {
    func fetch(completion: @escaping ([Pet]) -> Void) {
        guard let request = ApiRequest.pets(page: petsPage).makeRequest(withToken: token) else {
            return
        }

        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else { return }
            guard let petsContainer = try? self.decoder.decode(PetsContainer.self, from: data) else {
                return
            }
            
            self.petsPage += 1
            DispatchQueue.main.async {
                completion(petsContainer.pets)
            }
        }
        
        task.resume()
    }
    
    func fetch(completion: @escaping ([Organization]) -> Void) {
        guard let request = ApiRequest.organizations.makeRequest(withToken: token) else {
            return
        }
        
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data else { return }
            guard let organizationContainer = try? self.decoder.decode(OrganizationContainer.self, from: data) else {
                return
            }
            
            DispatchQueue.main.async {
                completion(organizationContainer.organizations)
            }
        }
        
        task.resume()
    }
}

struct Token: Decodable {
    let value: String
    
    enum CodingKeys: String, CodingKey {
        case value = "access_token"
    }
}
