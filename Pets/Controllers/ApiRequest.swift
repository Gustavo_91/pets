//
//  ApiRequest.swift
//  Pets
//
//  Created by Gustavo Pirela on 17/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import Foundation

enum ApiRequest {
    case pets(page: Int)
    case organizations
    case token
    
    func makeRequest(withToken token: Token?) -> URLRequest? {
        guard let url = url, let token = token else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("Bearer " + token.value, forHTTPHeaderField: "Authorization")
        return request
    }
    
    func makeTokenRequest() -> URLRequest? {
        guard let url = url else { return nil }
        
        switch self {
        case .token:
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
            let parameters = [
                "grant_type": "client_credentials",
                "client_id": "apqRnjKbHtko7qEApwwk1ZQIozdNiwx87Fe301DGmYN3sq8tmY",
                "client_secret": "trc8acAtGS8AEEOm47nH8suDb4Hxsbp3PnrkAUUD"
            ]
            request.composeBody(with: parameters)
            return request
        default:
            return nil
        }

    }
    
}

private extension  ApiRequest {
    var queryItems: [URLQueryItem] {
        var items = [URLQueryItem]()
        
        switch self {
        case let .pets(page: page):
            items = [
                URLQueryItem(name: "location", value: "ny"),
                URLQueryItem(name: "status", value: "adoptable"),
                URLQueryItem(name: "page", value: String(page))
            ]
        case .organizations:
            items = [
                URLQueryItem(name: "location", value: "40.778532, -73.957451"),
                URLQueryItem(name: "distance", value: "1")
            ]
        case .token:
            items = []
        }
        
        return items
    }
    
    var url: URL? {
        var components: URLComponents? = URLComponents()
        
        switch self {
        case .pets:
            components = URLComponents(string: "https://api.petfinder.com/v2/animals")
        case .organizations:
            components = URLComponents(string: "https://api.petfinder.com/v2/organizations")
        case .token:
            components = URLComponents(string: "https://api.petfinder.com/v2/oauth2/token")
        }
        
        components?.queryItems = queryItems
        return components?.url
    }
}

