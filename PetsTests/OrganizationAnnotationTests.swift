//
//  OrganizationAnnotationTests.swift
//  PetsTests
//
//  Created by Gustavo Pirela on 17/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import XCTest
import MapKit
@testable import Pets

class OrganizationAnnotationTests: XCTestCase {
    
    var organization: Organization!
    var sut: OrganizationAnnotation!
    
    override func setUp() {
        super.setUp()
        organization = Organization(name: "Rescue Dogs Rock NYC", distance: 0.8774)
        sut = OrganizationAnnotation(organization: organization)
    }
    
    override func tearDown() {
        organization = nil
        sut = nil
        super.tearDown()
    }
    
    func test_titleGetter_withOrganization_shouldReturnOrganizationName() {
        guard let name = sut.title else {
            XCTFail("Title property is nil")
            return
        }
        
        XCTAssertEqual(name, organization.name)
    }
    
    func test_distanceBetweenCenterAndLocation_shouldBeAtLeastOneMileFromTheOrganizationDistance() {
        let latitude = sut.coordinate.latitude
        let longitude = sut.coordinate.longitude
        
        let location = CLLocation(latitude: latitude, longitude: longitude)
        
        let distanceInMiles = location.distance(from: Constant.Map.center) / 1609.24
        XCTAssertEqual(distanceInMiles, organization.distance, accuracy: 1)
    }
    
}
