//
//  PetsTests.swift
//  PetsTests
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import XCTest
import Foundation
@testable import Pets

class PetTests: XCTestCase {

    func test_initializer_withJson_shouldReturnAnInstance() {
        let json = """
            {
                "age": "Baby",
                "gender": "Male",
                "size": "Medium",
                "name": "Stanley",
                "type": "cat",
                "photos": [
                    {
                        "small": "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/47125385/1/?bust=1579138146&width=100",
                        "medium": "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/47125385/1/?bust=1579138146&width=300",
                        "large": "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/47125385/1/?bust=1579138146&width=600",
                        "full": "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/47125385/1/?bust=1579138146"
                    },
                    {
                        "small": "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/47125385/2/?bust=1579138146&width=100",
                        "medium": "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/47125385/2/?bust=1579138146&width=300",
                        "large": "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/47125385/2/?bust=1579138146&width=600",
                        "full": "https://dl5zpyw5k3jeb.cloudfront.net/photos/pets/47125385/2/?bust=1579138146"
                    }
                ],
        }
        """
        guard let data = json.data(using: .utf8) else {
            XCTFail("Bad formatting of the json text")
            return
        }
        let decoder = JSONDecoder()
        
        let pet = try? decoder.decode(Pet.self, from: data)
        
        XCTAssertNotNil(pet)
    }
    
    func test_initializer_withJson_withPhotosFieldEmpty_shouldReturnAnInstance() {
        let json = """
            {
                "age": "Baby",
                "gender": "Male",
                "size": "Medium",
                "name": "Stanley",
                "type": "cat",
                "photos": [],
            }
        """
        guard let data = json.data(using: .utf8) else {
            XCTFail("Bad formatting of the json text")
            return
        }
        let decoder = JSONDecoder()
        
        let pet = try? decoder.decode(Pet.self, from: data)
        
        XCTAssertNotNil(pet)
    }
    
}
