//
//  OrganizationContainerTests.swift
//  PetsTests
//
//  Created by Gustavo Pirela on 17/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import XCTest
@testable import Pets

class OrganizationContainerTests: XCTestCase {

    func test_initializer_withJson_shouldReturnAnInstance() {
        let json = """
            {
                "organizations": [
                    {
                        "name": "Saving Paws Rescue, Inc.",
                        "distance": 35.0784
                    },
                    {
                        "name": "Saving Paws Rescue, Inc.",
                        "distance": 35.0784
                    }
                ],
                "pagination": {
                    "count_per_page": 20,
                    "total_count": 8,
                    "current_page": 1,
                    "total_pages": 1
                }
            }
        """
        guard let data = json.data(using: .utf8) else {
            XCTFail("Bad formatting of the json text")
            return
        }
        let decoder = JSONDecoder()
        
        let organization = try? decoder.decode(OrganizationContainer.self, from: data)
        
        XCTAssertNotNil(organization)
    }
    
}
