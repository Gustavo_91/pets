//
//  OrganizationTests.swift
//  PetsTests
//
//  Created by Gustavo Pirela on 16/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import XCTest
@testable import Pets

class OrganizationTests: XCTestCase {

    func test_initializer_withJson_shouldReturnAnInstance() {
        let json = """
        {
            "name": "Saving Paws Rescue, Inc.",
            "distance": 35.0784
        }
        """
        guard let data = json.data(using: .utf8) else {
            XCTFail("Bad formatting of the json text")
            return
        }
        let decoder = JSONDecoder()
        
        let organization = try? decoder.decode(Organization.self, from: data)
        
        XCTAssertNotNil(organization)
    }
    
}
