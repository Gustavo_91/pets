//
//  PetsContainerTests.swift
//  PetsTests
//
//  Created by Gustavo Pirela on 17/01/2020.
//  Copyright © 2020 me. All rights reserved.
//

import XCTest
@testable import Pets

class PetsContainerTests: XCTestCase {

    func test_initializer_withJson_shouldReturnAnInstance() {
        let json = """
            {
                "animals": [
                    {
                        "age": "Baby",
                         "gender": "Male",
                         "size": "Medium",
                         "name": "Stanley",
                         "type": "cat",
                         "photos": [],
                     },
                     {
                          "age": "Baby",
                          "gender": "Male",
                          "size": "Medium",
                          "name": "Stanley",
                          "type": "cat",
                          "photos": [],
                      }
                ] ,
                "pagination": {
                    "count_per_page": 20,
                    "total_count": 2336,
                    "current_page": 1,
                    "total_pages": 117,
                    "_links": {
                        "next": {
                            "href": "/v2/animals?location=ny&status=adoptable&page=2"
                        }
                    }
                }
            }
        """
        guard let data = json.data(using: .utf8) else {
            XCTFail("Bad formatting of the json text")
            return
        }
        let decoder = JSONDecoder()
        
        let petsContainer = try? decoder.decode(PetsContainer.self, from: data)
        
        XCTAssertNotNil(petsContainer)
    }

}
